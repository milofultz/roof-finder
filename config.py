# You can find more details here: https://pypi.org/project/python-craigslist/

# The subdomain of the Craigslist area that you are searching within
# e.g. https://portland.craigslist.org/ => "portland"
site = "newyork"

# The section of housing within which you want to search
# e.g. all => https://portland.craigslist.org/search/hhh => "hhh"
#      apartments => https://portland.craigslist.org/search/apa => "apa"
#      real estate => https://portland.craigslist.org/search/rea => "rea"
category = "apa"

# Any of the filters listed in the README
filters = {
    "zip_code": 11217,
    "search_distance": 5,
    "max_price": 10000,
    "min_price": 100,
    "min_bedrooms": 2,
    "housing_type": ["house", "townhouse", "in-law"],
    "has_image": True,
}

