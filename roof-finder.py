from config import site, category, filters
from craigslist import CraigslistHousing as house


options = dict(site=site, category=category, filters=filters)
area = house(**options)

print("Getting results...")

results = [result for result in area.get_results(sort_by="newest", geotagged=True)]

results = sorted(results, key=lambda h: h["geotag"][0])

output = ""
for result in results:
    output += result["name"] + "\n" + result["url"] + "\n"

print("Writing to `output.txt` file...")

with open("output.txt", "w") as f:
    f.write(output)

print("Complete!")

