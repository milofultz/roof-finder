# Roof Finder

## What is it

Collect all housing options from various ideal locations on Craigslist and put them in a file so I don't have to do it myself.


## Usage

* Install all dependencies with `pip3 install -r requirements.txt`
* Fill out the `config.py` with any options necessary
* Run `python3 roof-finder.py`
* Open the `output.txt` file to view all results

**WARNING**: Using this program on too broad of a search, or a search with too many results will result in you getting your IP blocked for a certain amount of time. I recommend keeping your searches fairly specific to avoid this.


## Filters

You can find out more information about the filters by going to [the docs of the craigslist Python package](https://pypi.org/project/python-craigslist/).

### Base filters:
* query = ...
* search_titles = True/False
* has_image = True/False
* posted_today = True/False
* bundle_duplicates = True/False
* search_distance = ...
* zip_code = ...

### Section specific filters:
* private_room = True/False
* private_bath = True/False
* cats_ok = True/False
* dogs_ok = True/False
* min_price = ...
* max_price = ...
* min_ft2 = ...
* max_ft2 = ...
* min_bedrooms = ...
* max_bedrooms = ...
* min_bathrooms = ...
* max_bathrooms = ...
* no_smoking = True/False
* is_furnished = True/False
* wheelchair_acccess = True/False
* housing_type = 'apartment', 'condo', 'cottage/cabin', 'duplex', 'flat', 'house', 'in-law', 'loft', 'townhouse', 'manufactured', 'assisted living', 'land'
* laundry = 'w/d in unit', 'w/d hookups', 'laundry in bldg', 'laundry on site', 'no laundry on site'

The information below was pulled from running this in the Python3 REPL:

```python3
>>> from craigslist import CraigslistHousing
>>> CraigslistHousing.show_filters()
```

## Future Improvements

* Utilizing `requests` and regular timeouts to ensure no IP blocks occur

